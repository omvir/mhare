<?php 
  
  define('PATH', dirname(__FILE__));
  session_start();
if(!isset($_SESSION['s_userId']))
{
    header("Location:login.php");
}

  require('header.php');
  require('config.php');
  
 //$con=mysql_connect($host,$user,$pass) or die(mysql_error());
 //mysql_select_db($db,$con) or die(mysql_error());
  $result=mysql_query("SELECT * FROM kaarigar ") or die(mysql_error());
  $cnt =mysql_num_rows($result);
  if($cnt>0){
  	$i=1;
  	$karigar = "var karigar = [";
  	while($data=mysql_fetch_row($result)){
  		if($i!=$cnt ){
  			$karigar .="{value:'".$data[1]."',data:'".$data[0]."',wastage:'".$data[2]."'},";
  		}else{
  			$karigar .="{value:'".$data[1]."',data:'".$data[0]."',wastage:'".$data[2]."'}";
  		}
  		$i++;
  	}
  	$karigar.="];";


  	$result=mysql_query("SELECT * FROM item ") or die(mysql_error());
  	$cnt =mysql_num_rows($result);
  	if($cnt>0){
  		$i=1;
  		$item = "var item = [";
  		while($data=mysql_fetch_row($result)){
  			if($i!=$cnt ){
  				$item .="{value:'".$data[1]."',data:'".$data[0]."'},";
  			}else{
  				$item .="{value:'".$data[1]."',data:'".$data[0]."'}";
  			}
  			$i++;
  		}
  	$item.="];";
  	}	 		 
  	
  	
?>
<link href="css/autocomplete.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
  <?php	echo $karigar;?>
  <?php	echo $item;?>
</script>
  <?php   
  }
  //mysql_close($con);

?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="right-side strech"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Transaction</h1>
    </section>
    
    <!-- Main content -->
    <form action="saveTransaction.php" method="post">
    <section class="content">
      <div class="row">
        <div class="col-md-12"> 
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Add Transaction</h3>
            </div>
	           Type :<select name="ttype" class="form-control"  style="width: 300px;" value="Debit"><option value="Debit"	>Debit</option><option value="Credit">Credit</option></select>
	           Name : <input name="kname" id="kname" class="form-control" type="text" style="width: 300px;">
	           <input type="hidden" id="wt" name="wt" value="">
	           <input type="hidden" id="kid" name="kid" value="">
	           Date : <input name="date" id="date" class="form-control" type="text" style="width: 300px;">
	           <br>
	           <table id="psList" class="table table-bordered table-hover">
			      	<thead>
			        	<tr>
							<th>Item</th>
							<th>Gross Wt.</th>
							<th>Less</th>
							<th>Net Wt.</th>
							<th>Touch</th>
							<th>Wastage</th>
							<th>Fine</th>       
							<th>In Touch</th>
					   </tr>
			        </thead>
			        <tbody>
						<tr id="1" class="repeat">
				            <td><input type="text" name="item[]"  id="item"    dataIndex="1" class="form-control onlynum qntbox aitem"></td>
				            <td><input type="text" onchange="TotalGrossWt();" style="text-align:right" name="grosswt[]" id="grosswt" dataIndex="1" class="form-control onlynum qntbox"></td>
				            <td><input type="text" onchange="TotalLess();" style="text-align:right" name="less[]"    id="less"    dataIndex="1" class="form-control onlynum qntbox"></td>
				            <td><input type="text" style="text-align:right" name="netwt[]"   id="netwt"   dataIndex="1" class="form-control netwt" readonly="readonly"></td>
				            <td><input type="text" onchange="tch();"  style="text-align:right" name="touch[]" id="touch"   dataIndex="1" class="form-control onlynum addrmdrow"></td>
				            <td><input type="text" onblur="wastag(1);"  style="text-align:right" name="wastage[]" d="wastage" dataIndex="1" class="form-control onlynum amtbox" ></td>
				            <td><input type="text" readonly="readonly"  onchange=""  style="text-align:right" name="fine[]"    id="fine"    dataIndex="1" class="form-control onlynum wgtbox" ></td>
							<td><input type="text" onchange="TotalInTouch();" onblur="appendRow();" style="text-align:right" name="intouch[]" id="intouch" dataIndex="1" class="form-control onlynum qntbox"></td>
			        	</tr>
			    	</tbody>
			        <tfoot>
				        <tr>
				            <th>Total</th>
				            <th id="grosswtTotal"  style="text-align:right" ></th>
				            <th id="lessTotal" style="text-align:right" ></th>
				            <th id="netwtTotal" style="text-align:right" ></th>
				            <th style="text-align:right" ></th>
				            <th style="text-align:right"></th>
				            <th id="fineTotal" style="text-align:right"></th>
							<th id="intouchTotal" style="text-align:right" ></th>
				        </tr>
					</tfoot>
		      </table>
		      <input type="submit" value="Save" >	
          </div>
          <!-- /.box --> 
        </div>
      </div>
    </section>
    </form>
    <!-- /.content --> 
  </aside>
  <!-- /.right-side --> 
</div>
<!-- ./wrapper --> 
<?php require('footer.php'); ?>
  <script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
  <script type="text/javascript" src="js/currency-autocomplete.js"></script>
  <script src="js/transaction.js" type="text/javascript"></script> 