<?php
class Serp
{
		function __construct()
		{
		}
		public function insertDomain($domain)
		{
			global $db;
			$current_date=date("Y-m-d H:i:s");
			mysql_query("INSERT into serp_module (domain,datetime) VALUES('$domain','$current_date')");
			if(mysql_insert_id() > 0)
				return mysql_insert_id();
			else
				return;
		}
		public function insertKeywords($id,$keywords)
		{
		
			global $db;
			$current_date=date("Y-m-d H:i:s");
			if(is_array(explode(",",$keywords))){
				$keywordArray=explode(",",$keywords);
			}
			else{
				$keywordArray=array('0'=>$keywords);
			}
			
			foreach($keywordArray as $key=>$value){
				
				mysql_query("INSERT into serp_keywords (domain_id,keywords,last_check) VALUES($id,'$value','$current_date')");
			}
			
			
		}
		public function get_serp_data(){
			
			global $db;
			$returnarray=array();
			$Res=mysql_query("SELECT * FROM serp_module");
			
			if(mysql_affected_rows()){
				
				return $Res;
			}	
			else{
				return null;
			}
			
		}
		public function get_keywords_from_domain_id($id){
		
			global $db;
			$returnarray=array();
			$Res=mysql_query("SELECT * FROM serp_keywords WHERE domain_id=$id");
			
			if(mysql_affected_rows()){
				
				while($row=mysql_fetch_assoc($Res)){
					$returnarray[]=$row['keywords'];
				}
				
				return implode(',',$returnarray);
			}	
			else{
				return null;
			}
		}
		public function count_keyword_from_domain_id($id){
			global $db;
			$returnarray=array();
			$Res=mysql_query("SELECT * FROM serp_keywords WHERE domain_id=$id");
			
			if(mysql_affected_rows()){
				
				return mysql_num_rows($Res);
			}	
			else{
				return null;
			}
		}
		public function get_domain_by_id($id){
			global $db;
			$Res=mysql_query("SELECT * FROM serp_module WHERE id=$id");
			if(mysql_affected_rows()){
				$res=mysql_fetch_object($Res);
				return $res;
			}
			else{
				return;
			}
		}
		public function get_keywords_by_domain_id($id){
			global $db;
			$returnarray=array();			
			$Res=mysql_query("SELECT * FROM serp_keywords WHERE domain_id=$id");
			
			if(mysql_affected_rows()){
				return $Res;
			}	
			else{
				return null;
			}
		}
		public function get_page_rank_by_domain_id($id){
			global $db;
			$returnarray=array();
			$Res=mysql_query("SELECT * FROM serp_keywords WHERE domain_id=$id");
			
			if(mysql_affected_rows()){
				
				while($row=mysql_fetch_assoc($Res)){
					if($row['rank'] !="0")
						$returnarray[]=$row['rank'];
				}
				
				return min($returnarray);
			}	
			else{
				return null;
			}
		}
		public function get_keyword_data_from_keyword_id($id){
			global $db;
			$returnarray=array();
			
			$Res=mysql_query("SELECT rank,datetime FROM serp_keyword_rank WHERE keyword_id=$id having rank>0");
			
			if(mysql_affected_rows()){
				while($row=mysql_fetch_assoc($Res)){
					$returnarray['rank'][]=$row['rank'];
					$returnarray['datetime'][]=$row['datetime'];
				}	
				 $mindate=min($returnarray['datetime']);
				 $maxdate=max($returnarray['datetime']);
				$maxrank=min($returnarray['rank']);
				$minrank=max($returnarray['rank']);
				$days=$this->numberofdays($mindate,$maxdate);
				return array("max"=>$maxrank,"min"=>$minrank,"days"=>$days);
			}	
			else{
				return null;
			}	
		}
		public function get_keyword_data_by_kid($id){
			global $db;
			$returnarray=array();
			
			$Res=mysql_query("SELECT * FROM serp_keyword_rank WHERE keyword_id=$id having rank>0");
			
			if(mysql_affected_rows()){
			
				return $Res;
			}	
			else{
				return null;
			}	
		}
		public function numberofdays($mindate,$maxdate){
		$mindate=date("Y-m-d",strtotime($mindate));
		$maxdate=date("Y-m-d",strtotime($maxdate));
			$startTimeStamp = strtotime($mindate);
			$endTimeStamp = strtotime($maxdate);

			$timeDiff = abs($endTimeStamp - $startTimeStamp);

			$numberDays = $timeDiff/86400;  // 86400 seconds in one day

			// and you might want to convert to integer
			$numberDays = intval($numberDays);
			return $numberDays;
		}	
		
}

?>