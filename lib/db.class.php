<?php

class DataBase {

    public $host;
    private $user;
    private $pass;
    private $dbname;

    function __construct($host,$user,$pass,$dbname) {
	
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->dbname = $dbname;
		
    }

    function connect_db() {
        $conn = mysql_connect($this->host, $this->user, $this->pass) or die('Error! No Connection Established');
		
        $db = mysql_select_db($this->dbname, $conn) or die('Error! No Database Found');
    }
	function is_login(){
		if(!empty($_SESSION['u_id']) && !empty($_SESSION['u_username']) && !empty($_SESSION['u_role'])){
			return true;
		}
		else{
			return false;
		}
	}
}