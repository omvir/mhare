$(function() {
	
	$( "#ps-entry" ).submit(function() {
  	if($("#customerId").val() == 0 && $("#customerName").val() == "" ){
			alert("Select Customer.");
			$("#customerId").focus();
			return false;
		}
		if($("#purchaseDate").val() == ""){
			alert("Enter Date.");
			$("#purchaseDate").focus();
			return false;
		}
		if($("#designId").val() == 0 && $("#designName").val() == "" ){
			alert("Select Design.");
			$("#designId").focus();
			return false;
		}
		$("#ps-entry :input").attr("disabled", false);
	});
	
	$( "#sell" ).click(function() {
		$("#trnType").val("sell");
	});
	
	$( "#purchase" ).click(function() {
		$("#trnType").val("purchase");
	});	
	
	$(".onlynum").keydown(function (e) {
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
          (e.keyCode == 65 && e.ctrlKey === true) || 
          (e.keyCode >= 35 && e.keyCode <= 39)) {
          return;
      }
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });
  
  var myArray = [];  
  var count = $("#psList >tbody >tr").length;
	$('body').on('focusout', '.rtebox', function() {
	    cVal = $(this).val();
	    var lastrow = $("#psList >tbody >tr:last").attr('id');
	    name = $(this).attr('name');
	    dataVal = $(this).attr("dataindex");
	    if(name == "rate[]" && cVal != ""){
		    count++;
		    
		    EnteredRate = $(this).val();
		    $("#urate_"+dataVal).val(EnteredRate/100);
		    calTotal();
		    
		    $("#psList >tbody tr:first").clone().find("input,select").each(function() {
	        $(this).attr({
	            'id': function(_, id) {
								newid = id.split("_");
                return newid[0] + "_" + count;
	            },
	            'dataIndex': function(_, id) {
                return count;
	            }
	        });
		    }).end().appendTo("#psList >tbody").show();
		    
		    $("#psList >tbody tr:last").find("input").each(function() {
		    	$(this).val('');
		    });
		    
		    $("#psList >tbody tr:last").find("select").each(function() {
		      $(this).val('0');
		    });
		    
		    $("#psList >tbody >tr:last").attr('id',count);
		    $("#psList >tbody >tr:last td:first").html(count);
	    }
	});

	$("[data-mask]").inputmask();
	
});

$('body').on('change', '.qtybox', function() {
	dataVal = $(this).attr("dataindex");
	$.ajax({
    url: 'ajax.php',
    type: 'POST',
    dataType: "json",
    data: { act: 'getColor', qualityId: $(this).val() },
    success: (function(data){
    	$("#colorId_"+dataVal).empty();
    	$("#colorId_"+dataVal).append($("<option></option>").val(0).html('Color'));
    	$.each(data, function () {
      	$("#colorId_"+dataVal).append($("<option></option>").val(this['colorId']).html(this['color']));
      });
    })
	});
});

$('body').on('change', '.clrbox', function() {
	dataVal = $(this).attr("dataindex");
	$.ajax({
    url: 'ajax.php',
    type: 'POST',
    dataType: "json",
    data: { act: 'getShape', qualityId: $('#qualityId_'+dataVal).val(), colorId: $(this).val() },
    success: (function(data){
    	$("#shapeId_"+dataVal).empty();
    	$("#shapeId_"+dataVal).append($("<option></option>").val(0).html('Shape'));
    	$.each(data, function () {
      	$("#shapeId_"+dataVal).append($("<option></option>").val(this['shapeId']).html(this['shape']));
      });
    })
	});
});

$('body').on('change', '.spebox', function() {
	dataVal = $(this).attr("dataindex");
	$.ajax({
    url: 'ajax.php',
    type: 'POST',
    dataType: "json",
    data: { act: 'getSize', qualityId: $('#qualityId_'+dataVal).val(), colorId: $('#colorId_'+dataVal).val(), shapeId: $(this).val() },
    success: (function(data){
    	$("#sizeId_"+dataVal).empty();
    	$("#sizeId_"+dataVal).append($("<option></option>").val(0).html('Size'));
    	$.each(data, function () {
      	$("#sizeId_"+dataVal).append($("<option></option>").val(this['sizeId']).html(this['size']));
      });
    })
	});
});

$('body').on('change', '.szebox', function() {
	dataVal = $(this).attr("dataindex");
	$.ajax({
    url: 'ajax.php',
    type: 'POST',
    dataType: "json",
    data: { act: 'getSizeData', sizeId: $(this).val() },
    success: (function(data){
    	if(data[0].rate != ""){
	      $("#rate_"+dataVal).val(data[0].rate);
	      $("#urate_"+dataVal).val(data[0].rate/100);
	    }else{
	    	$("#rate_"+dataVal).val(0);
	      $("#urate_"+dataVal).val(0);
	    }
    })
	});
});

$('body').on('focusout', '.qntbox', function() {
	dataVal = $(this).attr("dataindex");
	if($(this).val() != "" && $("#urate_"+dataVal).val() != ""){
		$("#amount_"+dataVal).val( parseFloat($(this).val()) * parseFloat($("#urate_"+dataVal).val()) );
	}else{
		$("#amount_"+dataVal).val('');
	}
	calTotal();
});

$('body').on('change', '.wgtbox', function() {
	calTotal();
});

function calTotal(){
	totQty = totAmt = totWgh = 0;
	var count = $("#psList >tbody >tr").length;
	for(i=1;i<=count;i++){
		if($("#qty_"+i).val() != "") totQty = totQty + parseFloat($("#qty_"+i).val())
		if($("#amount_"+i).val() != "") totAmt = totAmt + parseFloat($("#amount_"+i).val())
		if($("#weight_"+i).val() != "") totWgh = totWgh + parseFloat($("#weight_"+i).val())
	}
	$("#tQty").text(totQty);
	$("#tAmount").text(totAmt.toFixed(2));
	$("#tWeight").text(totWgh.toFixed(2));
}
