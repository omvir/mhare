$(document).ready(function(){
  calculateTotal();
});

function getWastage(){
	//alert("ok");
	var getData = {
		kaarigar_id: $("#selectKaarigar").val()
	};
	$.ajax({
    type:'POST',
    url:'ajax/getWastage.php',
    data: getData,
    success: function(data){
		//alert(data);
         if(data!="NO"){
            $("#Wastage").val(data);
         } else if(data=="NO") {
            $("#Wastage").val("");
         }
    }
  });
}

function calculateNetWeight() {
  var result1 = parseFloat(parseFloat($("#GrossWeight").val()) - parseFloat($("#Less").val()) + parseFloat($("#InTouch").val())).toFixed(3);
  var result2 = parseFloat(parseFloat($("#NetWeight").val()) * (parseFloat($("#Touch").val()) + parseFloat($("#Wastage").val())) / 100 ).toFixed(3);
  $("#NetWeight").val(result1);
  $("#Fine").val(result2);
}

function calculateTotal()
{
  var grossSUM = 0;
  var lessSUM = 0;
  var intouchSUM = 0;
  var netweightSUM = 0;
  var fineSUM = 0;
  $("#transactionTbody tr").each(function(i,row){
      grossSUM = parseFloat(grossSUM) + parseFloat($(row).find(".grossweight").html());
      lessSUM = parseFloat(lessSUM) + parseFloat($(row).find(".less").html());
      intouchSUM = parseFloat(intouchSUM) + parseFloat($(row).find(".intouch").html());
      netweightSUM = parseFloat(netweightSUM) + parseFloat($(row).find(".netweight").html());
      fineSUM = parseFloat(fineSUM) + parseFloat($(row).find(".fine").html());
  });

  
  $("#totalGross").html(parseFloat(grossSUM).toFixed(3));
  $("#totalLess").html(parseFloat(lessSUM).toFixed(3));
  $("#totalIntouch").html(parseFloat(intouchSUM).toFixed(3));
  $("#totalNetweight").html(parseFloat(netweightSUM).toFixed(3));
  $("#totalFine").html(parseFloat(fineSUM).toFixed(3));
}

function fillGridChange()
{
	getWastage();
  var kid = $("#selectKaarigar").val();
  var kdate = $("#year").val() + "-" + $("#month").val() + "-" + $("#day").val();
  var kfunction = $("#Function").val();
  var insertData = {
    date: kdate,
    kaarigar_id: kid,
    Function: kfunction,
    Item : $("#Item").val(),GrossWeight : $("#GrossWeight").val(),Less : $("#Less").val(),InTouch : $("#InTouch").val(),
    NetWeight : $("#NetWeight").val(),Touch : $("#Touch").val(),Wastage : $("#Wastage").val(),Fine : $("#Fine").val()
  };
  $.ajax({
    type:'POST',
    url:'ajax/view.php',
    data: insertData,
    success: function(data){
      //alert(data);
         if(data == "YES"){
            //$("#transactionTbody").html(data);  
            //calculateTotal();
            clearTextFields();
            alert("transaction added successfuly");
         } else if(data=="NO") {
            $("#transactionTbody").html("");
			//calculateTotal();
         }
    }
  });
}

function fillGrid()
{
	getWastage();
  var kid = $("#selectKaarigar").val();
  var kdate = $("#year").val() + "-" + $("#month").val() + "-" + $("#day").val();
  var kfunction = $("#Function").val();
  var insertData = {
    date: kdate,
    kaarigar_id: kid,
    Function: kfunction
  };
  $.ajax({
    type:'POST',
    url:'ajax/view.php',
    data: insertData,
    success: function(data){
      //alert(data);
         if(data!="NO"){
            $("#transactionTbody").html(data);  
            calculateTotal();
         } else if(data=="NO") {
            $("#transactionTbody").html("");
			      calculateTotal();
         }
    }
  });
}

function clearTextFields()
{
    $("#Item").val("");
    $("#GrossWeight").val("");
    $("#Less").val("");
    $("#InTouch").val("");
    $("#NetWeight").val("");
    $("#Touch").val("");
    $("#Fine").val("");
}

function insertTransaction(){
  var inDate = $("#year").val() +"-"+ $("#month").val() +"-"+ $("#day").val();
  var kaarigarID = $("#selectKaarigar").val();
  var insertData = $("#form-insert").serialize() + "&Date=" + inDate + "&kaarigar_id=" + kaarigarID;

  $.ajax({
    type:'POST',
    url:'ajax/insert.php',
    data: insertData,
    success: function(data){
      //alert(data);
         if(data=="YES"){
            fillGrid();
            calculateTotal();
            clearTextFields();
         }else{
             alert("can't insert the row");
         }
    }
  })
}

function deleteTransaction(thisObj){
  var id = $(thisObj).closest("tr").find(".transactionID").val()
  //alert(id);
  $.ajax({
    type:'POST',
    url: 'ajax/delete.php',
    data:{TransactionDetailID:id},
    success: function(data){
      //alert(data);
         if(data=="YES"){
            $(thisObj).closest("tr").fadeOut().remove();
            calculateTotal();
         }else{
             alert("can't delete the row");
         }
    }
  })
}