jQuery(document.body).on('blur', '.netwt', function(event) {
	var grWt    = new Number($(this).closest('tr').children('td:eq(1)').find(':input').val());
	var less    = new Number($(this).closest('tr').children('td:eq(2)').find(':input').val());
	var inTouch = new Number($(this).closest('tr').children('td:eq(5)').find(':input').val());
	//var total   = grWt - less + inTouch;
	var total   = grWt - less;
	$(this).closest('tr').children('td:eq(3)').find(':input').val(total.toFixed(7));
	TotalNetWt();
});
 
function appendRow() {
	
	var n = $('#psList tbody tr:last').attr('id');
	var rowId  = new Number(n)+1;
	var tr ='<tr id="'+rowId+'" class="repeat">';
	tr +='<td><input autofocus   type="text" name="item[]" id="item"  dataIndex="1" class="form-control onlynum qntbox item"></td>';
	tr +='<td><input type="text" name="grosswt[]" onchange="TotalGrossWt();"  style="text-align:right" id="grosswt" dataIndex="1" class="form-control onlynum qntbox"></td>';
	tr +='<td><input type="text" name="less[]"  onchange="TotalLess();"  id="less"   style="text-align:right" dataIndex="1" class="form-control onlynum qntbox"></td>';
	
	tr +='<td><input type="text" name="netwt[]" readonly="readonly" onchange="TotalNetWt();"  id="netwt" style="text-align:right"  dataIndex="1" class="form-control netwt"></td>';
	tr +='<td><input type="text" name="touch[]"  onchange="tch();"xz  id="touch" style="text-align:right" dataIndex="1" class="form-control onlynum addrmdrow"></td>';
	
	tr +='<td><input type="text" name="wastage[]" onblur="wastag('+rowId+');" id="wastage" style="text-align:right" dataIndex="1" value="'+$('#wt').val()+'" class="form-control onlynum amtbox"></td>';
	tr +='<td><input type="text" name="fine[]"  readonly="readonly"  id="fine"  onkeyup="" style="text-align:right"   dataIndex="1" class="form-control onlynum wgtbox"></td>';
	tr +='<td><input type="text" name="intouch[]" onblur="appendRow('+(rowId++)+');" onchange="TotalInTouch();"  id="intouch" style="text-align:right" dataIndex="1" class="form-control onlynum qntbox"></td>';
	tr +='</tr>';
    $("#psList").append(tr);
    $("#psList tbody tr:last td:first").find(':input').focus();
}
 
function TotalGrossWt(){
	var sum = 0;
	$( 'input[name^="grosswt"]' ).each( function( i , e ) {
	    var v = new Number( $( e ).val() );
	    $( e ).val(v.toFixed(3));
	        sum += v;
	} );
	$('#grosswtTotal').text(sum.toFixed(3));
}
function TotalLess(){
	var sum = 0;
	$( 'input[name^="less"]' ).each( function( i , e ) {
	    var v =  new Number($( e ).val());
	    $( e ).val(v.toFixed(3));
	    sum += v;
	} );
	$('#lessTotal').text(sum.toFixed(3));
}
function TotalInTouch(){
	var sum = 0;
	$( 'input[name^="intouch"]' ).each( function( i , e ) {
	    var v =  new Number( $( e ).val() );
	    $( e ).val(v.toFixed(3));
	    sum += v;
	});
	
	$('#intouchTotal').text(sum.toFixed(3));
//		TotalNetWt();
}
function TotalNetWt(){
	var sum = 0;
	$( 'input[name^="netwt"]' ).each( function( i , e ) {
	    var v =  new Number( $( e ).val() );
	    $( e ).val(v.toFixed(3));
	    sum += v;
	} );
	$('#netwtTotal').text(sum.toFixed(3));
}
function tch() {
	$( 'input[name^="touch"]' ).each( function( i , e ) {
	    var v =  new Number( $( e ).val() );
	    $( e ).val(v.toFixed(3));
	} );
}

function wastag(rowId){
	$('#'+rowId).each(function(i, tr) {
		  var netWt   = new Number($(this).find("td").eq(3).find('input').val());  
		  var touch   = new Number($(this).find("td").eq(4).find('input').val());  
		  var wastage = new Number($(this).find("td").eq(5).find('input').val());  
		  var total  = (netWt * (touch+wastage))/100;
	      $(this).find("td").eq(6).find('input').val(total.toFixed(7));  
	});

	/*$( 'input[name^="wastage"]').each( function( i , e ) {
	    var v =  new Number( $( e ).val() );
	    $( e ).val(v.toFixed(3));
	});*/
		
		TotalFine();
			
}

function TotalFine(){
	var sum = 0;
	$( 'input[name^="fine"]' ).each( function( i , e ) {
	    var v =  new Number( $( e ).val() );
	    $( e ).val(v.toFixed(3));
	    sum += v;
	} );
	$('#fineTotal').text(sum.toFixed(3));
}