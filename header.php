<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8">
<title>AdminLTE | Dashboard</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
	$("document").ready(function() {
    $("#datepicker").datepicker(
					{dateFormat:"yy-mm-dd",appendText:"(YEAR-MONTH-DATE)"});
    //alert('jQuery is working');
});
</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		  
        <![endif]-->
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header"> <a href="../index.html" class="logo"> 
  <!-- Add the class icon to your logo image or logo icon to add the margining --> 
  AdminLTE </a>
  <div class="left-side sidebar-offcanvas top-menu"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu">
	  
	
	
		<?php if($_SERVER['REQUEST_URI']=="/mhare/kaarigar.php") { ?>
        <li class="active"><a href="kaarigar.php"><span>Kaarigar</span></a></li>
		<?php } else { ?>
		  <li ><a href="kaarigar.php"><span>Kaarigar</span></a></li>
		<?php } ?>
		
		<?php if($_SERVER['REQUEST_URI']=="/mhare/vepaari.php") { ?>
        <li class="active"><a href="vepaari.php"><span>Vepaari</span></a></li>
		<?php } else { ?>
		  <li ><a href="vepaari.php"><span>Vepaari</span></a></li>
		<?php } ?>
		
		 <?php if($_SERVER['REQUEST_URI']=="/mhare/item.php") { ?>
        <li class="active"><a href="item.php" ><span>Item</span></a></li>
		<?php } else { ?>
		  <li ><a href="item.php"><span>Item</span></a></li>
		<?php } ?>
		
		 <?php if($_SERVER['REQUEST_URI']=="/mhare/transaction.php") { ?>
        <li class="active"><a href="transaction.php"><span>Transaction</span></a></li>
		<?php } else { ?>
		  <li><a href="transaction.php"><span>Transaction</span></a></li>
		<?php } ?>
     
		<?php if($_SERVER['REQUEST_URI']=="/mhare/amount.php") { ?>
		        <li class="active" ><a href="amount.php?ptype=k"><span>K Amount</span></a></li>
		        <li class="active" ><a href="amount.php?ptype=v"><span>V Amount</span></a></li>
		<?php } else { ?>
		         <li><a href="amount.php?ptype=k"><span>K Amount</span></a></li>
		         <li><a href="amount.php?ptype=v"><span>V Amount</span></a></li>
		<?php } ?>
 
	    <?php if($_SERVER['REQUEST_URI']=="/mhare/fine.php") { ?>
				<li class="active"><a href="fine.php?ptype=k"><span>K Fine</span></a></li>
				<li class="active"><a href="fine.php?ptype=v"><span>V Fine</span></a></li>
		<?php } else { ?>
		       <li><a href="fine.php?ptype=k"><span>K Fine</span></a></li>
		       <li><a href="fine.php?ptype=v"><span>V Fine</span></a></li>
		<?php } ?>
		
		<?php if($_SERVER['REQUEST_URI']=="/mhare/cutRate.php") { ?>
		     <li class="active"><a href="cutRate.php?ptype=k"><span>K Cut Rate</span></a></li>    
		     <li class="active"><a href="cutRate.php?ptype=v"><span>V Cut Rate</span></a></li>    
   		<?php } else { ?>
		       <li><a href="cutRate.php?ptype=k"><span>K Cut Rate</span></a></li>
		       <li><a href="cutRate.php?ptype=v"><span>V Cut Rate</span></a></li>
		<?php } ?>
        
      
      </ul>
    </section>
    <!-- /.sidebar --> 
  </div>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation"> 
    <!-- Sidebar toggle button--> 
    <a href="#" class="navbar-btn sidebar-toggle nav-icon" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
    <div class="navbar-right">
      <ul class="nav navbar-nav">
        
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i> <span>User Name <i class="caret"></i></span> </a>
          <ul class="dropdown-menu">
            <li> <a href="#">Change Password</a> </li>
            <li> <a href="signout.php">Sign out</a> </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>