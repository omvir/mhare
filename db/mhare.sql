-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 19, 2014 at 05:04 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mhare`
--
CREATE DATABASE IF NOT EXISTS `mhare` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mhare`;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `ItemID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ItemID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`ItemID`, `Name`) VALUES
(1, 'Bali'),
(2, 'Chain');

-- --------------------------------------------------------

--
-- Table structure for table `kaarigar`
--

CREATE TABLE IF NOT EXISTS `kaarigar` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Wastage` varchar(5) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kaarigar`
--

INSERT INTO `kaarigar` (`ID`, `Name`, `Phone`, `Wastage`) VALUES
(1, 'Kishor', '1234567890', '2.50'),
(2, 'Gupta', '9999999999', '1.50');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `TransactionID` int(11) NOT NULL AUTO_INCREMENT,
  `KaarigarID` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Item` varchar(100) NOT NULL,
  `GrossWeight` double(9,3) NOT NULL,
  `Less` double(9,3) NOT NULL,
  `InTouch` double(9,3) NOT NULL,
  `NetWeight` double(9,3) NOT NULL,
  `Touch` double(8,2) NOT NULL,
  `Wastage` double(5,2) NOT NULL,
  `Fine` double(9,3) NOT NULL,
  PRIMARY KEY (`TransactionID`),
  KEY `KaarigarID` (`KaarigarID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`TransactionID`, `KaarigarID`, `Date`, `Item`, `GrossWeight`, `Less`, `InTouch`, `NetWeight`, `Touch`, `Wastage`, `Fine`) VALUES
(15, 2, '2014-11-18', 'adwd', 12.000, 0.000, 0.000, 0.000, 0.00, 0.00, 0.000),
(16, 2, '2014-11-18', 'dwaaw', 2.000, 0.000, 0.000, 0.000, 0.00, 0.00, 0.000);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
