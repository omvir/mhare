-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 24, 2014 at 07:24 AM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `omvirco_mhare`
--

-- --------------------------------------------------------

--
-- Table structure for table `dongel`
--

CREATE TABLE IF NOT EXISTS `dongel` (
`dongle_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `dongel`
--

INSERT INTO `dongel` (`dongle_id`, `date_time`) VALUES
(1, '2014-11-19 07:46:26'),
(2, '2014-11-19 07:48:16'),
(3, '2014-11-19 07:48:19'),
(4, '2014-11-19 07:48:22'),
(5, '2014-11-19 07:48:28'),
(6, '2014-11-19 07:48:49'),
(7, '2014-11-19 07:48:51'),
(8, '2014-11-19 07:51:06'),
(9, '2014-11-19 08:44:56'),
(10, '2014-11-19 08:45:32'),
(11, '2014-11-19 09:07:03'),
(12, '2014-11-19 09:08:32'),
(13, '2014-11-19 09:15:10'),
(14, '2014-11-19 09:15:19'),
(15, '2014-11-19 09:29:38'),
(16, '2014-11-23 08:15:14');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
`ItemID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`ItemID`, `Name`) VALUES
(1, 'Bali'),
(7, 'dfdf');

-- --------------------------------------------------------

--
-- Table structure for table `kaarigar`
--

CREATE TABLE IF NOT EXISTS `kaarigar` (
`ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Wastage` varchar(5) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `kaarigar`
--

INSERT INTO `kaarigar` (`ID`, `Name`, `Phone`, `Wastage`) VALUES
(1, 'Kishor', '1234567890', '2.50'),
(21, 'hggh', '454545', '3'),
(22, 'K1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`ID` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `userId`, `password`, `email`) VALUES
(1, 'admin', 'admin', 'admin@mhare.com');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
`TransactionID` int(11) NOT NULL,
  `KaarigarID` int(11) NOT NULL,
  `createdDate` date NOT NULL,
  `Item` varchar(100) NOT NULL,
  `GrossWeight` double(9,3) NOT NULL,
  `Less` double(9,3) NOT NULL,
  `InTouch` double(9,3) NOT NULL,
  `NetWeight` double(9,3) NOT NULL,
  `Touch` double(8,2) NOT NULL,
  `Wastage` double(5,2) NOT NULL,
  `Fine` double(9,3) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`TransactionID`, `KaarigarID`, `createdDate`, `Item`, `GrossWeight`, `Less`, `InTouch`, `NetWeight`, `Touch`, `Wastage`, `Fine`) VALUES
(16, 2, '2014-11-18', 'dwaaw', 2.000, 0.000, 0.000, 0.000, 0.00, 0.00, 0.000),
(17, 21, '2014-11-18', 'chain', 123.000, 2.000, 3.000, 124.000, 89.00, 3.00, 114.080),
(18, 21, '2014-11-20', 'bali', 140.000, 2.890, 8.900, 146.010, 78.00, 3.00, 118.268),
(20, 1, '2014-11-20', 'chain', 123.000, 1.000, 12.000, 134.000, 23.00, 2.50, 34.170),
(22, 1, '2014-11-21', 'fsdf', 111.000, 12.000, 12.000, 111.000, 12.00, 2.50, 16.095),
(23, 21, '2014-11-18', 'bali', 130.000, 1.281, 1.210, 129.929, 78.08, 3.00, 105.346);

-- --------------------------------------------------------

--
-- Table structure for table `vepaari`
--

CREATE TABLE IF NOT EXISTS `vepaari` (
`ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Wastage` varchar(5) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `vepaari`
--

INSERT INTO `vepaari` (`ID`, `Name`, `Phone`, `Wastage`) VALUES
(1, 'test', '455454', '2.3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dongel`
--
ALTER TABLE `dongel`
 ADD PRIMARY KEY (`dongle_id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
 ADD PRIMARY KEY (`ItemID`);

--
-- Indexes for table `kaarigar`
--
ALTER TABLE `kaarigar`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
 ADD PRIMARY KEY (`TransactionID`), ADD KEY `KaarigarID` (`KaarigarID`);

--
-- Indexes for table `vepaari`
--
ALTER TABLE `vepaari`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dongel`
--
ALTER TABLE `dongel`
MODIFY `dongle_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
MODIFY `ItemID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kaarigar`
--
ALTER TABLE `kaarigar`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
MODIFY `TransactionID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `vepaari`
--
ALTER TABLE `vepaari`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
